Scala Play 2.3 Guice Example
==============================

Example of a scala play 2.3 based application using [Google Guice](https://github.com/google/guice).

The pieces that make it work:

* Route classes starting with `@controller`. Play uses the `getControllerInstance` method in
  the global to find an instance for controller classes prefixed with this annotation.
* Implement `getControllerInstance` as something like:

```scala
  override def getControllerInstance[A](controllerClass: Class[A]) = {
    println(s"Getting controller $controllerClass")
    logger.info(s"Creating logger for $controllerClass")
    injector.getInstance(controllerClass)
  }
```
* Annotate injectable things with one of the javax.inject annontations like `@javax.inject.Singleton`.
  Add an `@Inject()` annotation to the class constructor. Add a list of injectable things after the
  controller. See `Service1` for an example.
* Testing can proceed using constructor injection. Injectable classes can still be newed up in test instances.

