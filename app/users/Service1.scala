package users

import javax.inject.{Inject, Singleton}

import scala.util.{Failure, Success, Try}

@Singleton
class Service1 @Inject()(userRepository: UserRepository) {
  def findUser(userId: String): Try[User] =
    userRepository.findUser(userId)
      .map {
        Success(_)
      }.getOrElse(Failure(new RuntimeException("No Such User!")))
}
