package users

import javax.inject.Singleton

@Singleton
class UserRepository {

  def findUser(userId: String): Option[User] = if (userId == "sean") {
    Some(User("Sean", "Mooney"))
  } else {
    None
  }
}
