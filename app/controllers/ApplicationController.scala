package controllers

import javax.inject.Inject

import play.api.mvc.{Action, Controller}
import users.Service1

class ApplicationController @Inject()(service1: Service1) extends Controller {
  def index = Action {
    Ok("hello world")
  }

  def getUser(userId: String) = Action {
    service1.findUser(userId)
      .map(Ok(_).as("Json"))
      .getOrElse(NotFound)
  }
}
