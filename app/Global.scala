import com.google.inject.{Guice, Injector}
import play.api.{GlobalSettings, Logger}

/**
 * The global configuration.
 */
object Global extends GlobalSettings {

  /**
   * The Guice dependencies injector.
   */
  var injector: Injector = _
  
  override def onStart(app: play.api.Application) = {
    super.onStart(app)
    // Now the configuration is read and we can create our Injector.
    injector = Guice.createInjector()
  }

  /**
   * Loads the controller classes with the Guice injector,
   * in order to be able to inject dependencies directly into the controller.
   *
   * @param controllerClass The controller class to instantiate.
   * @return The instance of the controller class.
   * @throws Exception if the controller couldn't be instantiated.
   */
  override def getControllerInstance[A](controllerClass: Class[A]) = {
    injector.getInstance(controllerClass)
  }

}
