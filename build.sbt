import play.PlayScala

name := "play-2_3-guice-demo"

version := "1.0"

libraryDependencies ++= Seq(
  "com.google.inject" % "guice" % "3.0",
  "javax.inject" % "javax.inject" % "1",
  "org.scalactic" %% "scalactic" % "3.0.1",
  "org.mockito" % "mockito-core" % "1.9.5" % "test",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  cache
)

lazy val root = (project in file(".")).enablePlugins(PlayScala)
