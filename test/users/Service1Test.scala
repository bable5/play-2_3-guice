package users

import org.mockito.Matchers.anyString
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{BeforeAndAfter, FunSuite}

import scala.util.Success

class Service1Test extends FunSuite with MockitoSugar with BeforeAndAfter {

  var userRepository: UserRepository = _

  var service: Service1 = _

  before {
    userRepository = mock[UserRepository]

    service = new Service1(userRepository)
  }

  test("Should convert Some user to a successful user") {
    val user = User("some", "body")
    givenUser(user)

    assert(service.findUser("some user id") === Success(user))
  }

  test("Should convert None to a Failure") {
    givenNoUser()

    assert(service.findUser("some user id").isFailure)
  }

  private def givenUser(user: User) {
    doReturn(Some(user))
      .when(userRepository)
      .findUser(anyString)
  }

  private def givenNoUser() {
    doReturn(None)
      .when(userRepository)
      .findUser(anyString)
  }
}
